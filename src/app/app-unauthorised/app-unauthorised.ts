import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-unauthorised",
  templateUrl: "./app-unauthorised.component.html",
  styleUrls: ["./app-unauthorised.component.css"],
})
export class NotAuthorisedComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
