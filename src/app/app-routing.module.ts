import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PokemonHomeComponent } from "./app-pokemon-home/app-pokemon-home.component";
import { PokemonDetailComponent } from "./app-pokemon-detail/app-pokemon-detail.component";
import { PokemonProductCreateComponent } from "./app-pokemon-product-create/app-pokemon-product-create.component";
import { PokemonProductListComponent } from "./app-pokemon-product-list/app-pokemon-product-list.component";
import { PokemonCustomGuard } from "./app-pokemon-custom.guard";
import { HomePageComponent } from "./app-home-page/app-home-page.component";
import { NotAuthorisedComponent } from "./app-unauthorised/app-unauthorised";

const routes: Routes = [
  { path: "pokemonHomeComponent", component: PokemonHomeComponent },
  { path: "notAuthorisedComponent", component: NotAuthorisedComponent },
  { path: "homePageComponent", component: HomePageComponent },
  { path: "pokemonDetailComponent", component: PokemonDetailComponent },
  {
    path: "pokemonProductCreateComponent",
    component: PokemonProductCreateComponent,
  },
  {
    path: "pokemonProductListComponent",
    component: PokemonProductListComponent,
    canActivate: [PokemonCustomGuard],
  },
  { path: "", redirectTo: "/homePageComponent", pathMatch: "full" }, // redirect to `first-component`
  { path: "**", redirectTo: "/homePageComponent", pathMatch: "full" }, // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
