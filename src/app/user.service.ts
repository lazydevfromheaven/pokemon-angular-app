import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class UserService {
  //By Default user is an admin
  isAdmin = true;
}
