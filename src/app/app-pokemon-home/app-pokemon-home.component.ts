import { Component, OnInit } from "@angular/core";
import { FetchAPIResService } from "../fetch-api-response-service";

@Component({
  selector: "app-pokemon-home",
  templateUrl: "./app-pokemon-home.component.html",
  styleUrls: ["./app-pokemon-home.component.css"],
})
export class PokemonHomeComponent implements OnInit {
  pokemonList: any;
  limit = 30;
  offset = 0;
  pokemonsLoaded: boolean;

  constructor(public FetchAPIResService: FetchAPIResService) {}

  ngOnInit(): void {
    this.pokemonsLoaded = false;
    this.getPokemonList();
  }
  attachImageToPokemon(pokemonList) {
    this.pokemonsLoaded = false;
    let url: string;
    let pokemonDetail;
    pokemonList.forEach((pokemon) =>
      this.FetchAPIResService.getPokemonDetail(
        "https://pokeapi.co/api/v2/pokemon/" + pokemon.name
      ).subscribe(
        (resp: any) => {
          pokemonDetail = resp?.sprites?.front_default;
          pokemon.imageUrl = pokemonDetail;
        },
        (error) => {}
      )
    );
    this.pokemonsLoaded = true;
    return pokemonList;
  }

  getPokemonList() {
    this.FetchAPIResService.getPokemonList(this.limit, this.offset).subscribe(
      (resp: any) => {
        this.pokemonList = this.attachImageToPokemon(resp.results);
      }
    );
  }

  onPrevious() {
    this.pokemonsLoaded = false;
    if (this.offset !== 0) {
      this.offset -= 30;
      this.FetchAPIResService.getPokemonList(this.limit, this.offset).subscribe(
        (resp: any) => {
          this.pokemonList = this.attachImageToPokemon(resp.results);
        }
      );
    }
  }

  onNext() {
    this.pokemonsLoaded = false;
    this.offset += 30;
    this.FetchAPIResService.getPokemonList(this.limit, this.offset).subscribe(
      (resp: any) => {
        this.pokemonList = this.attachImageToPokemon(resp.results);
      }
    );
  }
}
