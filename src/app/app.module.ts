import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PokemonHomeComponent } from "./app-pokemon-home/app-pokemon-home.component";
import { PokemonDetailComponent } from "./app-pokemon-detail/app-pokemon-detail.component";
import { PokemonProductCreateComponent } from "./app-pokemon-product-create/app-pokemon-product-create.component";
import { PokemonProductListComponent } from "./app-pokemon-product-list/app-pokemon-product-list.component";
import { HomePageComponent } from "./app-home-page/app-home-page.component";
import { CreateProductChildComponent } from "./app-product-create/app-product-create.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NotAuthorisedComponent } from "./app-unauthorised/app-unauthorised";
import { TwoDigitDecimalNumberDirective } from "./two-digit-decimal-number.directive";

@NgModule({
  declarations: [
    AppComponent,
    PokemonHomeComponent,
    PokemonDetailComponent,
    PokemonProductCreateComponent,
    PokemonProductListComponent,
    HomePageComponent,
    CreateProductChildComponent,
    NotAuthorisedComponent,
    TwoDigitDecimalNumberDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
