import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-pokemon-product-list",
  templateUrl: "./app-pokemon-product-list.component.html",
  styleUrls: ["./app-pokemon-product-list.component.css"],
})
export class PokemonProductListComponent implements OnInit {
  productList = [];

  constructor() {}

  ngOnInit(): void {
    this.productList = JSON.parse(localStorage.getItem("productList"));
  }
}
